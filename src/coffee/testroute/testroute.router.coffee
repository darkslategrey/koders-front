null


### @ngInject ###
appRun = (routerHelper) ->
  routerHelper.configureStates getStates(), '/'

getStates = ->
  [
    state: 'testroute'
    config:
      url: '/testroute'
      templateUrl: 'app/angular/testroute/testroute.html'
      controller: 'TestRouteController'
      controllerAs: 'vm'
      title: 'TestRoute'
      settings:
        nav: 1
        content: '<i class="fa fa-dashboard"></i> Dashboard'
  ]

'use strict'
angular.module('app.testroute').run appRun
