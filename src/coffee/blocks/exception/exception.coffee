null

### @ngInject ###
exception = (logger) ->
  catcher = (message) ->
    (reason) ->
      logger.error message, reason

  service = catcher: catcher
  return service

'use strict'
angular.module('blocks.exception').factory 'exception', exception

