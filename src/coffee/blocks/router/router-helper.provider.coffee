routerHelperProvider = ($locationProvider, $stateProvider, $urlRouterProvider) ->
  # jshint validthis:true 
  config =
    docTitle: `undefined`
    resolveAlways: {}

  $locationProvider.html5Mode true
  @configure = (cfg) ->
    angular.extend config, cfg

  ### @ngInject ###
  RouterHelper = ($location, $rootScope, $state, logger) ->

    #/////////////
    configureStates = (states, otherwisePath) ->
      states.forEach (state) ->
        state.config.resolve = angular.extend(state.config.resolve or {}, config.resolveAlways)
        $stateProvider.state state.state, state.config

      if otherwisePath and not hasOtherwise
        hasOtherwise = true
        $urlRouterProvider.otherwise otherwisePath

    handleRoutingErrors = ->
      # Route cancellation:
      # On routing error, go to the dashboard.
      # Provide an exit clause if it tries to do it twice.
      $rootScope.$on '$stateChangeError', (event, toState, toParams, fromState, fromParams, error) ->
        return  if handlingStateChangeError
        stateCounts.errors++
        handlingStateChangeError = true
        destination = (toState and (toState.title or toState.name or toState.loadedTemplateUrl)) or 'unknown target'
        msg = 'Error routing to ' + destination + '. ' + (error.data or '') + '. <br/>' + (error.statusText or '') + ': ' + (error.status or '')
        logger.warning msg, [toState]
        $location.path '/'

    init = ->
      handleRoutingErrors()
      updateDocTitle()

    getStates = ->
      $state.get()
      
    updateDocTitle = ->
      $rootScope.$on '$stateChangeSuccess', (event, toState, toParams, fromState, fromParams) ->
        stateCounts.changes++
        handlingStateChangeError = false
        title = config.docTitle + ' ' + (toState.title or '')
        $rootScope.title = title # data bind to <title>

    handlingStateChangeError = false
    hasOtherwise = false
    stateCounts =
      errors: 0
      changes: 0

    service =
      configureStates: configureStates
      getStates: getStates
      stateCounts: stateCounts

    init()
    return service

  @$get = RouterHelper
  return
  
'use strict'
angular.module('blocks.router').provider 'routerHelper', routerHelperProvider

