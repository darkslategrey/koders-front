null
  
### @ngInject ###
toastrConfig = (toastr) ->
  toastr.options.timeOut = 4000
  toastr.options.positionClass = 'toast-bottom-right'

### @ngInject ###
configure = ($logProvider, routerHelperProvider, exceptionHandlerProvider) ->
  $logProvider.debugEnabled true  if $logProvider.debugEnabled
  exceptionHandlerProvider.configure config.appErrorPrefix
  routerHelperProvider.configure docTitle: config.appTitle + ': '

'use strict'
core = angular.module('app.core')
core.config toastrConfig

config =
  appErrorPrefix: '[Koders Error] '
  appTitle: 'Koders'

core.value 'config', config
core.config configure


