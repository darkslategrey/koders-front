null

### @ngInject ###
appRun = (routerHelper) ->
  routerHelper.configureStates getStates(), '/'

getStates = ->
  [
    state: 'home'
    config:
      url: '/'
      templateUrl: 'app/angular/home/home.html'
      controller: 'HomeController'
      controllerAs: 'vm'
      title: 'Home'
      settings:
        nav: 1
        content: '<i class="fa fa-dashboard"></i> Dashboard'
  ]

'use strict'
angular.module('app.home').run appRun
