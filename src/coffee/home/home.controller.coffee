null

### @ngInject ###
HomeController = ($q, $scope, logger) ->
  vm = this
  $scope.data =
    selectedIndex: 0
    secondLocked: true
    secondLabel: 'Item Two'

  $scope.next = ->
    $scope.data.selectedIndex = Math.min($scope.data.selectedIndex + 1, 2)
    return

  $scope.previous = ->
    $scope.data.selectedIndex = Math.max($scope.data.selectedIndex - 1, 0)
    return


'use strict'
angular.module('app.home').controller 'HomeController', HomeController

