'use strict';

var gulp = require('gulp');
var $    = require('gulp-load-plugins')({
  pattern: ['gulp-*', 'main-bower-files', 'uglify-save-license', 'del']
});

gulp.task('inject', function () {
    var target      = gulp.src('src/index.slim');
    var sources     = gulp.src('src/app/angular/**/*.js').pipe($.angularFilesort());
    var dest        = gulp.dest('./src');
    var transformFc = function(filePath, file) {
	return 'script type="text/javascript" src="'+filePath.toString().replace('/src/', './')+'"';
    };
    var injectOpts  = {starttag: '/ inject:js', endtag: '/ endinject', transform: transformFc }
    return target.pipe($.inject(sources, injectOpts)).pipe(dest);
});

gulp.task('inject-css', function () {
    var target      = gulp.src('src/index.slim');
    var sources     = gulp.src('src/app/angular/**/*.css');
    var dest        = gulp.dest('./src');
    var transformFc = function(filePath, file) {
	return 'link rel="stylesheet" href="'+filePath.toString().replace('/src/', './')+'"';
    };
    var injectOpts  = {starttag: '/ inject:css', endtag: '/ endinject', transform: transformFc }
    return target.pipe($.inject(sources, injectOpts)).pipe(dest);
});
