'use strict';

var gulp = require('gulp');

gulp.task('watch', ['styles', 'inject', 'inject-css', 'slim', 'slim-angular', 'coffee'] ,function () { 
    gulp.watch('src/{app,components,coffee}/**/*.coffee', ['coffee']);
    gulp.watch('src/index.slim', ['slim']);
    gulp.watch('src/coffee/**/*.slim', ['slim-angular']);           
    gulp.watch('src/{app,components,angular}/**/*.scss', ['styles']);
    gulp.watch('src/{app,components}/**/*.js', ['scripts']);
    gulp.watch('src/app/angular/**/*.js', ['inject']);
    gulp.watch('src/assets/images/**/*', ['images']);
    gulp.watch('bower.json', ['wiredep']);
});
